Jupiter Starter Child Theme
=============

This child theme for the [Jupiter](http://themeforest.net/item/jupiter-multipurpose-responsive-theme/5177775?ref=coyotedesign) theme is what I use as a base every time I start a new Jupiter based website. It includes some useful functions and filters, some useful Jupiter element overrides, as well as a base custom js file, some initial general and Jupiter specific CSS written in SCSS, and various useful mixins and the [Neat](http://neat.bourbon.io/) for responsive styling.

Installation
-----------

0. Copy child theme contents into your wp-content/themes folder.
0. Open up style.css and place your own child theme name and replace screenshot.png with your own theme screenshot.
0. Download and install [WP-SCSS](https://wordpress.org/plugins/wp-scss/) or use your own compliler for library/scss/style.scss - if you use WP-SCSS you just need to point the plugin to library/scss/ and library/css for the two folders it uses, and have it auto enqueue the generated stylesheet. If you use a compiler, you'll need to include the .css output file in the themes main style.css file or enqueue it elsewhere.
0. Set your desired screen size variables for responsive styling at the top of library/scss/style.scss
0. Check out library/scss/_mixins.scss for useful mixins available.

Other Notes / Usage
-----------
* You can add any custom JS in library/js/custom.js and this will automatically be enqueued.
* In functions.php you'll find a bunch of useful functions - one that adds page slug and browser classes to each page/post, some useful Gravity Forms, WP SEO and Advanced Custom Fields functions, and finally a function to output the nececessary HTML for [Real Favicon Generator](https://realfavicongenerator.net/) generated favicons. All you have to do is generate your icons and then place them in library/favicons folder.
* There are some useful element overrides for the Jupiter Text Block and Fancy Title elements. I wanted to be able to control and set the text color in the Text Block, and I also added controls to add text shadows for both. There's an additional override for the blog element when using the Thumbnail layout option so that the title shows above the post meta, and some extra code to detect whether or not a post has a featured image and add a class if not along with some basic styling for this.
