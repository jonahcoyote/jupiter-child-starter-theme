<?php

/*-------------------------------------------------------------------------------
	Enqueue sassed styles from child theme
-------------------------------------------------------------------------------*/
function example_enqueue_styles() {
	wp_enqueue_style('child-theme', get_stylesheet_directory_uri() .'/library/css/style.css');
}
add_action('wp_enqueue_scripts', 'example_enqueue_styles');


/*-------------------------------------------------------------------------------
	Add JS
-------------------------------------------------------------------------------*/
add_action('wp_enqueue_scripts', 'load_scripts');
function load_scripts() {
    wp_enqueue_script('customjs', get_stylesheet_directory_uri(). '/library/js/custom.js', array('jquery'), '1.0', true);
}


/*-------------------------------------------------------------------------------
  Add Image Sizes
-------------------------------------------------------------------------------*/
//add_image_size( 'useful-resources', 215, 215, true );


/*-------------------------------------------------------------------------------
	Make Content Blocks Public
-------------------------------------------------------------------------------*/
function filter_content_block_init() {
	$content_block_public = true;
	return $content_block_public;
}
add_filter('content_block_post_type','filter_content_block_init');


/*-----------------------------------------------------------------------------------*/
/* Adds new body classes
/*-----------------------------------------------------------------------------------*/
add_filter('body_class', 'add_browser_classes');
function add_browser_classes($classes){

    if(is_singular()) {
      global $post;
      $classes[] = $post->post_name;
    }

    return $classes;
}


/*-------------------------------------------------------------------------------
  Hide ACF menu item from the admin menu
-------------------------------------------------------------------------------*/
function hide_admin_menu() {
  global $current_user;
  get_currentuserinfo();

  if($current_user->user_login != 'jonahcoyote') {
    echo '<style type="text/css">#toplevel_page_edit-post_type-acf{display:none;}</style>';
  }

}
add_action('admin_head', 'hide_admin_menu');


/*-------------------------------------------------------------------------------
	Add Default ACF Options Page
-------------------------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}


/*-------------------------------------------------------------------------------
  Set ACF Options Title
-------------------------------------------------------------------------------*/
if(function_exists('acf_set_options_page_title')) {
  acf_set_options_page_title( __('Other Options') );
}



/*-------------------------------------------------------------------------------
	Prevent Gravity Forms Anchor Hop
-------------------------------------------------------------------------------*/
add_filter('gform_confirmation_anchor', '__return_false');

/*-------------------------------------------------------------------------------
  Remove WP SEO Columns
-------------------------------------------------------------------------------*/
add_filter( 'wpseo_use_page_analysis', '__return_false' );


/*-------------------------------------------------------------------------------
  Modify GForms Default Validation Message
-------------------------------------------------------------------------------*/
add_filter("gform_validation_message", "change_message", 10, 2);
function change_message($message, $form){
  return "<div class='validation_error'>Please fill in the required fields.</div>";
}


/*-------------------------------------------------------------------------------
	Add Favicons
-------------------------------------------------------------------------------*/
add_action( 'init' , 'remove_jupiter_favicons' , 15 );
function remove_jupiter_favicons() {
  remove_action('wp_head', 'mk_apple_touch_icons', 2);
}

add_action('wp_head', 'add_favicons');
function add_favicons() { ?>

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); 	?>/library/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); 	?>/library/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); 	?>/library/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/library/favicons/manifest.json">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/library/favicons/safari-pinned-tab.svg" 	color="#5bbad5">
  <meta name="theme-color" content="#ffffff">

<?php }


/*-------------------------------------------------------------------------------
	Set Media File Fefaults
-------------------------------------------------------------------------------*/
add_action( 'after_setup_theme', 'default_attachment_display_settings' );
/**
 * Set the Attachment Display Settings "Link To" default to "none"
 *
 * This function is attached to the 'after_setup_theme' action hook.
 */
function default_attachment_display_settings() {
	//update_option( 'image_default_align', 'left' );
	update_option( 'image_default_link_type', 'file' );
	//update_option( 'image_default_size', 'large' );
}


/*-------------------------------------------------------------------------------
	Register Posts Types
-------------------------------------------------------------------------------*/
if ( ! function_exists('register_post_types') ) {

// Register Custom Post Type
function register_post_types() {

	$icon = version_compare(get_bloginfo('version'), '3.8', '>=') ? 'dashicons-products' : false;
	$labels = array(
		'name'                  => _x( 'Instec Products', 'Post Type General Name', 'instec' ),
		'singular_name'         => _x( 'Instec Product', 'Post Type Singular Name', 'instec' ),
		'menu_name'             => __( 'Instec Products', 'instec' ),
		'name_admin_bar'        => __( 'Instec Products', 'instec' ),
		'archives'              => __( 'Archives', 'instec' ),
		'parent_item_colon'     => __( 'Parent Item:', 'instec' ),
		'all_items'             => __( 'All Products', 'instec' ),
		'add_new_item'          => __( 'Add New Product', 'instec' ),
		'add_new'               => __( 'Add New', 'instec' ),
		'new_item'              => __( 'New Product', 'instec' ),
		'edit_item'             => __( 'Edit Product', 'instec' ),
		'update_item'           => __( 'Update Product', 'instec' ),
		'view_item'             => __( 'View Product', 'instec' ),
		'search_items'          => __( 'Search Products', 'instec' ),
		'not_found'             => __( 'Not found', 'instec' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'instec' ),
		'featured_image'        => __( 'Featured Image', 'instec' ),
		'set_featured_image'    => __( 'Set featured image', 'instec' ),
		'remove_featured_image' => __( 'Remove featured image', 'instec' ),
		'use_featured_image'    => __( 'Use as featured image', 'instec' ),
		'insert_into_item'      => __( 'Insert into item', 'instec' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'instec' ),
		'items_list'            => __( 'Items list', 'instec' ),
		'items_list_navigation' => __( 'Items list navigation', 'instec' ),
		'filter_items_list'     => __( 'Filter items list', 'instec' ),
	);
	$args = array(
		'label'                 => __( 'Instec Products', 'instec' ),
		'description'           => __( 'A Post Type for Instec Products', 'instec' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'comments', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,
		'menu_icon'=> $icon,
		'rewrite'            => array( 'slug' => 'instec-product', 'with_front' => true ),
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'taxonomies' => array( 'ins_product_category', 'ins_product_tag' )
	);
	register_post_type( 'instec_product', $args );

}
//add_action( 'init', 'register_post_types', 0 );

}


/*-------------------------------------------------------------------------------
	Register Product Categories Taxonomy
-------------------------------------------------------------------------------*/
function custom_product_cat() {

	$labels = array(
		'name'                       => _x( 'Product Categories', 'Taxonomy General Name', 'instec' ),
		'singular_name'              => _x( 'Product Category', 'Taxonomy Singular Name', 'instec' ),
		'menu_name'                  => __( 'Product Categories', 'instec' ),
		'all_items'                  => __( 'All Items', 'instec' ),
		'parent_item'                => __( 'Parent Item', 'instec' ),
		'parent_item_colon'          => __( 'Parent Item:', 'instec' ),
		'new_item_name'              => __( 'New Item Name', 'instec' ),
		'add_new_item'               => __( 'Add New Item', 'instec' ),
		'edit_item'                  => __( 'Edit Item', 'instec' ),
		'update_item'                => __( 'Update Item', 'instec' ),
		'view_item'                  => __( 'View Item', 'instec' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'instec' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'instec' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'instec' ),
		'popular_items'              => __( 'Popular Items', 'instec' ),
		'search_items'               => __( 'Search Items', 'instec' ),
		'not_found'                  => __( 'Not Found', 'instec' ),
		'no_terms'                   => __( 'No items', 'instec' ),
		'items_list'                 => __( 'Items list', 'instec' ),
		'items_list_navigation'      => __( 'Items list navigation', 'instec' ),
	);
	$rewrite = array(
		'slug'                       => 'ins-product-cat',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'ins_product_category', array( 'instec_product' ), $args );

}
//add_action( 'init', 'custom_product_cat', 0 );


/*-------------------------------------------------------------------------------
	Register Product Tags Taxonomy
-------------------------------------------------------------------------------*/
function custom_product_tag() {

	$labels = array(
		'name'                       => _x( 'Product Tags', 'Taxonomy General Name', 'instec' ),
		'singular_name'              => _x( 'Product Tag', 'Taxonomy Singular Name', 'instec' ),
		'menu_name'                  => __( 'Product Tags', 'instec' ),
		'all_items'                  => __( 'All Items', 'instec' ),
		'parent_item'                => __( 'Parent Item', 'instec' ),
		'parent_item_colon'          => __( 'Parent Item:', 'instec' ),
		'new_item_name'              => __( 'New Item Name', 'instec' ),
		'add_new_item'               => __( 'Add New Item', 'instec' ),
		'edit_item'                  => __( 'Edit Item', 'instec' ),
		'update_item'                => __( 'Update Item', 'instec' ),
		'view_item'                  => __( 'View Item', 'instec' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'instec' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'instec' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'instec' ),
		'popular_items'              => __( 'Popular Items', 'instec' ),
		'search_items'               => __( 'Search Items', 'instec' ),
		'not_found'                  => __( 'Not Found', 'instec' ),
		'no_terms'                   => __( 'No items', 'instec' ),
		'items_list'                 => __( 'Items list', 'instec' ),
		'items_list_navigation'      => __( 'Items list navigation', 'instec' ),
	);
	$rewrite = array(
		'slug'                       => 'ins-product-tag',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'ins_product_tag', array( 'instec_product' ), $args );

}
//add_action( 'init', 'custom_product_tag', 0 );


/*-------------------------------------------------------------------------------
  Add Optimizely / Hotjar / Facebook Pixel
-------------------------------------------------------------------------------*/
function add_header_scripts() { ?>

	<!-- Hotjar Tracking Code for https://paddlemonster.com -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:208687,hjsv:5};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

<?php }
//add_action('wp_head', 'add_header_scripts');


/*-------------------------------------------------------------------------------
	Prevent TinyMCE from stripping out html
-------------------------------------------------------------------------------*/
function schema_TinyMCE_init($in)
{
    /**
     *   Edit extended_valid_elements as needed. For syntax, see
     *   http://www.tinymce.com/wiki.php/Configuration:valid_elements
     *
     *   NOTE: Adding an element to extended_valid_elements will cause TinyMCE to ignore
     *   default attributes for that element.
     *   Eg. a[title] would remove href unless included in new rule: a[title|href]
     */
    if(!empty($in['extended_valid_elements']))
        $in['extended_valid_elements'] .= ',';

    $in['extended_valid_elements'] .= '@[id|class|style|title|itemscope|itemtype|itemprop|datetime|rel],div,dl,ul,ol,dt,dd,li,span,a|rev|charset|href|lang|tabindex|accesskey|type|name|href|target|title|class|onfocus|onblur]';

    return $in;
}
add_filter('tiny_mce_before_init', 'schema_TinyMCE_init' );


/*-------------------------------------------------------------------------------
	Hide Admin Bar on Front End
-------------------------------------------------------------------------------*/
add_filter('show_admin_bar', '__return_false');


/*-------------------------------------------------------------------------------
	Move Yoast to bottom
-------------------------------------------------------------------------------*/
function yoasttobottom() {
	return 'low';
}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


/*-------------------------------------------------------------------------------
	Add Custom Menu Items
-------------------------------------------------------------------------------*/
function custom_menu_item_search($items, $args){
	if($args->theme_location == 'primary' && thegem_get_option('header_layout') !== 'overlay') {
		$items .= '<li class="menu-item test menu-item-account"><a href="' . get_bloginfo('url') . '/my-account/"></a></li>';
	}
	return $items;
}
//add_filter('wp_nav_menu_items', 'custom_menu_item_search', 10, 2);


/*-------------------------------------------------------------------------------
	Remove parent theme filter
-------------------------------------------------------------------------------*/
function remove_parent_filters() {
    remove_filter('wp_nav_menu_items', 'thegem_menu_item_search', 10, 2);
}
add_action( 'after_setup_theme', 'remove_parent_filters' );


/*-------------------------------------------------------------------------------
	Modify Default Gravity Forms Spinner
-------------------------------------------------------------------------------*/
add_filter( 'gform_ajax_spinner_url', 'tgm_io_custom_gforms_spinner' );
/**
 * Changes the default Gravity Forms AJAX spinner.
 *
 * @since 1.0.0
 *
 * @param string $src  The default spinner URL.
 * @return string $src The new spinner URL.
 */
function tgm_io_custom_gforms_spinner( $src ) {
    return get_stylesheet_directory_uri() . '/library/images/loader-1.svg';   
}


/*-------------------------------------------------------------------------------
	Custom Login CSS
-------------------------------------------------------------------------------*/
function custom_login() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/library/login/login-styles.css" />';
}
add_action('login_head', 'custom_login');


/*-------------------------------------------------------------------------------
	Custom Login URL
-------------------------------------------------------------------------------*/
function login_logo_url() {
    return 'https://lovethwild.com';
}
add_filter('login_headerurl', 'login_logo_url');