jQuery.noConflict();
jQuery(document).ready(function($){

	console.log('custom js loaded');

	/*-----------------------------------------------------------------------------------*/
	/* Add Classes
	/*-----------------------------------------------------------------------------------*/
	$("ul > li:first-child, ol > li:first-child, .hentry:first").addClass("first");
	$("ul > li:last-child, ol > li:last-child, .hentry:last").addClass("last");
  $("blockquote p:only-child").addClass("remove-margin");

	// Page section classes
	$("#mk-theme-container .mk-page-section-wrapper").eq(0).addClass("first-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(1).addClass("second-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(2).addClass("third-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(3).addClass("fourth-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(4).addClass("fifth-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(5).addClass("sixth-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(6).addClass("seventh-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(7).addClass("eighth-section");
	$("#mk-theme-container .mk-page-section-wrapper").eq(8).addClass("ninth-section");

  /*-------------------------------------------------------------------------------
	Remove Empty Paragraphs
  -------------------------------------------------------------------------------*/
	$('p').each(function() {
		var $this = $(this);
		if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
			$this.remove();
	});

  /*-------------------------------------------------------------------------------
	Better Pointer Events for Maps
  -------------------------------------------------------------------------------*/
  $('.maps').click(function () {
    $('.maps iframe').css("pointer-events", "auto");
  });

  $( ".maps" ).mouseleave(function() {
    $('.maps iframe').css("pointer-events", "none");
  });

	// Scroll to any link with hash
	/*
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
	*/

	// Custom scrollto animation
	$(".custom-scroll-to a").click(function() {
	    var targetDiv = $(this).attr('href');
	    $('html, body').animate({
	        scrollTop: $(targetDiv).offset().top - 50
	    }, 1000);
	});

});
