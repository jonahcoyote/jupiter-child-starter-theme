<?php
vc_map(array(
    "name" => __("Text Block", "mk_framework") ,
    "base" => "vc_column_text",
    'icon' => 'icon-mk-text-block vc_mk_element-icon',
    'wrapper_class' => 'clearfix',
    'category' => __('Content', 'mk_framework') ,
    'description' => __('A block of text with WYSIWYG editor', 'mk_framework') ,
    "params" => array(
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "heading" => __("Text", "mk_framework") ,
            "param_name" => "content",
            "value" => __("", "mk_framework") ,
            "description" => __("Enter your content.", "mk_framework")
        ) ,
        array(
            "type" => "textfield",
            "heading" => __("Title", "mk_framework") ,
            "param_name" => "title",
            "value" => "",
            "description" => __("You can optionally have global style title for this text block or leave this blank if you create your own title. Moreover you can disable this heading title's pattern divider using below option.", "mk_framework")
        ) ,
        array(
            "type" => "toggle",
            "heading" => __("Title Pattern?", "mk_framework") ,
            "param_name" => "disable_pattern",
            "value" => "true",
            "description" => __("If you want to remove the title pattern, disable this option.", "mk_framework")
        ) ,
        array(
            "type" => "theme_fonts",
            "heading" => __("Font Family", "mk_framework") ,
            "param_name" => "font_family",
            "value" => "",
            "description" => __("You can choose a font for this shortcode, however using non-safe fonts can affect page load and performance.", "mk_framework")
        ) ,
        array(
            "type" => "hidden_input",
            "param_name" => "font_type",
            "value" => "",
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "range",
            "heading" => __("Font Size", "mk_framework") ,
            "param_name" => "size",
            "value" => "16",
            "min" => "12",
            "max" => "70",
            "step" => "1",
            "unit" => 'px',
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "toggle",
            "heading" => __("Force Responsive Font Size?", "mk_framework") ,
            "param_name" => "force_font_size",
            "value" => "false",
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "range",
            "heading" => __("Font Size for Small Desktops", "mk_framework") ,
            "param_name" => "size_smallscreen",
            "value" => "0",
            "min" => "0",
            "max" => "70",
            "step" => "1",
            "unit" => 'px',
            "description" => __("For screens smaller than 1280px. If value is zero the font size not going to be affected.", "mk_framework"),
            "dependency" => array(
                'element' => "force_font_size",
                'value' => array(
                    'true'
                )
            )
        ) ,
        array(
            "type" => "range",
            "heading" => __("Font Size for Tablet", "mk_framework") ,
            "param_name" => "size_tablet",
            "value" => "0",
            "min" => "0",
            "max" => "70",
            "step" => "1",
            "unit" => 'px',
            "description" => __("For screens between 768 and 1024px. If value is zero the font size not going to be affected.", "mk_framework"),
            "dependency" => array(
                'element' => "force_font_size",
                'value' => array(
                    'true'
                )
            )
        ),
        array(
            "type" => "range",
            "heading" => __("Font Size for Mobile", "mk_framework") ,
            "param_name" => "size_phone",
            "value" => "0",
            "min" => "0",
            "max" => "70",
            "step" => "1",
            "unit" => 'px',
            "description" => __("For screens smaller than 768px. If value is zero the font size not going to be affected.", "mk_framework"),
            "dependency" => array(
                'element' => "force_font_size",
                'value' => array(
                    'true'
                )
            )
        ) ,
        array(
            "type" => "range",
            "heading" => __("Line Height", "mk_framework") ,
            "param_name" => "line_height",
            "value" => "100",
            "min" => "50",
            "max" => "150",
            "step" => "1",
            "unit" => '%',
            "description" => __("If hundred value is chosen, the default value set from theme options will be used. Use this option if you wish to override the line-height for this module by setting your own value.", "mk_framework")
        ) ,
        array(
            "type" => "dropdown",
            "heading" => __("Font Weight", "mk_framework") ,
            "param_name" => "font_weight",
            "value" => $font_weight,
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "dropdown",
            "heading" => __("Font Style", "mk_framework") ,
            "param_name" => "font_style",
            "value" => array(
                __('Default', "mk_framework") => "inherit",
                __('Normal', "mk_framework") => "normal",
                __('Italic', "mk_framework") => "italic",
            ) ,
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "dropdown",
            "heading" => __("Text Align", "mk_framework") ,
            "param_name" => "align",
            "width" => 150,
            "value" => array(
                __('Left', "mk_framework") => "left",
                __('Right', "mk_framework") => "right",
                __('Center', "mk_framework") => "center"
            ) ,
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "colorpicker",
            "heading" => __("Text Color", "mk_framework") ,
            "param_name" => "color",
            "value" => "",
            "description" => __("Set the color of the text in the block. Note, this will affect all text including headings.", "mk_framework")
        ),
        array(
            "type" => "range",
            "heading" => __("Max Width", "mk_framework") ,
            "param_name" => "max_width",
            "value" => "2000",
            "min" => "100",
            "max" => "2000",
            "step" => "1",
            "unit" => 'px',
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "toggle",
            "heading" => __("Center the text block?", "mk_framework") ,
            "param_name" => "center_align_toggle",
            "value" => "false",
            "description" => __("If setting a max width, you can also center the entire text block.", "mk_framework")
        ) ,
        array(
            "type" => "colorpicker",
            "heading" => __("Background", "mk_framework") ,
            "param_name" => "bg_color",
            "value" => "",
            "description" => __("Set the background color for the text block. This will also add a slight padding inside the block.", "mk_framework")
        ),
        array(
            "type" => "toggle",
            "heading" => __("Custom Padding?", "mk_framework") ,
            "param_name" => "inner_padding_toggle",
            "value" => "false",
            "description" => __("If setting a background, there will be a default padding of 20px, do you want to override that?", "mk_framework")
        ) ,
        array(
            "type" => "range",
            "heading" => __("Inner Padding", "mk_framework") ,
            "param_name" => "inner_padding",
            "value" => "20",
            "min" => "1",
            "max" => "200",
            "step" => "1",
            "unit" => 'px',
            "description" => __("", "mk_framework"),
            "dependency" => array(
                'element' => "inner_padding_toggle",
                'value' => array(
                    'true'
                )
            )
        ),
        array(
            "type" => "toggle",
            "heading" => __("Text Shadow?", "mk_framework") ,
            "param_name" => "text_shadow_toggle",
            "value" => "false",
            "description" => __("", "mk_framework")
        ) ,
        array(
            "type" => "colorpicker",
            "heading" => __("Text Shadow Color", "mk_framework") ,
            "param_name" => "text_shadow_color",
            "value" => "",
            "description" => __("Set the color of the text in the block. Note, this will affect all text including headings.", "mk_framework"),
            "dependency" => array(
                'element' => "text_shadow_toggle",
                'value' => array(
                    'true'
                )
            )
        ),
        array(
            "type" => "range",
            "heading" => __("Text Shadow Horizontal Size", "mk_framework") ,
            "param_name" => "text_shadow_horizontal_size",
            "value" => "1",
            "min" => "1",
            "max" => "10",
            "step" => "1",
            "unit" => 'px',
            "description" => __("The position of the horizontal shadow away from the text.", "mk_framework"),
            "dependency" => array(
                'element' => "text_shadow_toggle",
                'value' => array(
                    'true'
                )
            )
        ) ,
        array(
            "type" => "range",
            "heading" => __("Text Shadow Vertical Size", "mk_framework") ,
            "param_name" => "text_shadow_vertical_size",
            "value" => "1",
            "min" => "1",
            "max" => "10",
            "step" => "1",
            "unit" => 'px',
            "description" => __("The position of the vertical shadow away from the text.", "mk_framework"),
            "dependency" => array(
                'element' => "text_shadow_toggle",
                'value' => array(
                    'true'
                )
            )
        ) ,
        array(
            "type" => "range",
            "heading" => __("Text Shadow Blur Radius", "mk_framework") ,
            "param_name" => "text_shadow_blur_radius",
            "value" => "0",
            "min" => "1",
            "max" => "10",
            "step" => "1",
            "unit" => 'px',
            "description" => __("The blur radius of the shadow.", "mk_framework"),
            "dependency" => array(
                'element' => "text_shadow_toggle",
                'value' => array(
                    'true'
                )
            )
        ) ,
        array(
            "type" => "range",
            "heading" => __("Margin Bottom", "mk_framework") ,
            "param_name" => "margin_bottom",
            "value" => "0",
            "min" => "0",
            "max" => "200",
            "step" => "1",
            "unit" => 'px',
            "description" => __("", "mk_framework")
        ) ,
        $add_css_animations,
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "mk_framework") ,
            "param_name" => "el_class",
            "value" => "",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "mk_framework")
        ) ,
    )
));
