<?php
extract(shortcode_atts(array(
    'el_class' => '',
    'title' => '',
    'color' => '',
    'max_width' 						=> '',
    'center_align_toggle' 						=> 'false',
    'bg_color' => '',
    'inner_padding_toggle' => 'false',
  	'inner_padding' => '20',
    "size" 							=> '16',
  	'force_font_size'				=> 'false',
  	'size_smallscreen'				=> '0',
  	'size_tablet'					=> '0',
  	'size_phone'					=> '36',
  	"line_height" 					=> '100',
    "font_family" 					=> '',
    'font_style'	 				=> 'inherit',
    "font_type"						=> '',
    'font_weight' 					=> 'inherit',
    'text_shadow_toggle' => 'false',
    'text_shadow_color' => '#111111',
    'text_shadow_horizontal_size' => '1',
    'text_shadow_vertical_size' => '1',
    'text_shadow_blur_radius' => '0',
    'disable_pattern' => 'true',
    'margin_bottom' => 0,
    'align' => 'left',
    'animation' => '',
    'visibility' => '',
) , $atts));

$fancy_style = '';
$pattern = !empty($disable_pattern) ? $disable_pattern : 'true';
Mk_Static_Files::addAssets('vc_column_text');
