<?php
$path = pathinfo(__FILE__) ['dirname'];

include ($path . '/config.php');

$id = Mk_Static_Files::shortcode_id();

$class[] = 'mk-text-block';
$class[] = get_viewport_animation_class($animation);
$class[] = $visibility;
$class[] = $el_class;
$text_color = $color;

echo mk_get_fontfamily( "#text-block-", $id, $font_family, $font_type );

Mk_Static_Files::addCSS("
	#text-block-{$id} {
	  margin-bottom:{$margin_bottom}px;
	  text-align:{$align};
	}

	#text-block-{$id} p,
	#text-block-{$id} li {
		font-size: {$size}px !important;
		font-style: {$font_style};
		font-weight: {$font_weight};
		line-height: 1.5em !important;
	}
", $id);

if($text_color !='') {
	Mk_Static_Files::addCSS("
		#text-block-{$id} * {
			color:{$color};
		}
	", $id);
}

if( ! empty( $max_width ) ) {
	Mk_Static_Files::addCSS("
		#text-block-{$id} {
			max-width: {$max_width}px;
		}
	", $id);
}

if( !empty( $max_width ) && $center_align_toggle == 'true' ) {
	Mk_Static_Files::addCSS("
		#text-block-{$id} {
			margin: 0 auto;
		}
	", $id);
}

if($bg_color !='') {
	Mk_Static_Files::addCSS("
		#text-block-{$id} {
			background: {$bg_color};
    	padding: {$inner_padding}px;
		}
	", $id);
}

if( $line_height > 100 || $line_height < 100 || !isset($line_height) ) {
	Mk_Static_Files::addCSS("
		#text-block-{$id} p,
		#text-block-{$id} li {
			line-height: {$line_height}%;
		}
	", $id);
}

if($text_shadow_toggle == 'true') {
	Mk_Static_Files::addCSS("
		#text-block-{$id} p,
		#text-block-{$id} li {
			text-shadow: {$text_shadow_horizontal_size}px {$text_shadow_vertical_size}px {$text_shadow_blur_radius}px {$text_shadow_color};
		}
	", $id);
}

if ($force_font_size == 'true') {
	if($size_smallscreen != '0'){
		Mk_Static_Files::addCSS("
			@media handheld, only screen and (max-width: 1280px) {
				#text-block-{$id} p,
				#text-block-{$id} li {
					font-size: {$size_smallscreen}px;
				}
			}
		", $id);
	}
	if($size_tablet != '0') {
		Mk_Static_Files::addCSS("
			@media handheld, only screen and (min-width: 768px) and (max-width: 1024px) {
				#text-block-{$id} p,
				#text-block-{$id} li {
					font-size: {$size_tablet}px;
				}
			}
		", $id);
	}
	if($size_phone != '0') {
		Mk_Static_Files::addCSS("
			@media handheld, only screen and (max-width: 767px) {
				#text-block-{$id} p,
				#text-block-{$id} li {
					font-size: {$size_phone}px;
				}
			}
		", $id);
	}
}

?>

<div id="text-block-<?php echo $id; ?>" class="<?php echo implode(' ', $class); ?>">

	<?php mk_get_view('global', 'shortcode-heading', false, ['title' => $title, 'pattern' => $pattern, 'align' => $align]); ?>

	<?php echo wpb_js_remove_wpautop($content, true); ?>

	<div class="clearboth"></div>
</div>
